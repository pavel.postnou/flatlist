import { View, TouchableOpacity } from "react-native";
import React from "react";
import * as RootNavigation from "../navigation/RootNavigation";
import { darkStyle } from "./Item.dark.style";
import { lightStyle } from "./item.light.style";
import TextComponent from "../src/shared/components/Text";
import ImageComponent from "../src/shared/components/Image";

function Item({ data, users, theme }) {
  let styles;
  theme ? (styles = darkStyle) : (styles = lightStyle);
  const goInfo = () => {
    let send;
    users
      ? (send = users.results[data.id],
        RootNavigation.navigate("Describe", { send, theme }))
      : null;
  };

  return data ? (
    <TouchableOpacity onPress={() => goInfo()}>
      <View style={styles.user}>
        <View>
          <ImageComponent type="imgOne" source={{ uri: data.image }} />
        </View>
        <View style={styles.infoContainer}>
          <View style={styles.cityContainer}>
            <TextComponent type="naming" theme={theme} text="name: " />
            <TextComponent type="title" theme={theme} text={data.name} />
            <TextComponent type="naming" theme={theme} text="last name: " />
            <TextComponent type="title" theme={theme} text={data.lastName} />
          </View>
          <View style={styles.devider} />
          <View style={styles.cityContainer}>
            <TextComponent type="naming" theme={theme} text="country: " />
            <TextComponent type="country" theme={theme} text={data.country} />
            <TextComponent type="naming" theme={theme} text="city: " />
            <TextComponent type="country" theme={theme} text={data.city} />
          </View>
          <View style={styles.cityContainer}>
            <TextComponent type="naming" theme={theme} text="registered: " />
            <TextComponent type="date" theme={theme} text={data.date}/>
          </View>
        </View>
      </View>
    </TouchableOpacity>
  ) : null;
}

export default React.memo(Item);
