import { StyleSheet} from "react-native";
export const darkStyle = StyleSheet.create({
    user: {
      flexDirection: "row",
      height: 80,
      backgroundColor: "#2e2e2e",
      marginBottom: 5,
      marginHorizontal: 5,
      borderRadius: 10,
    },
    title: {
      fontSize: 15,
      color: "white",
      marginRight: 20,
    },
    city: {
      flex: 1,
      fontSize: 15,
      color: "yellow",
    },
    country: {
      fontSize: 15,
      color: "#57abff",
      marginRight: 20,
    },
    imgOne: {
      flex: 1,
      width: 80,
      resizeMode: "contain",
      marginVertical: 5,
      borderRadius: 10,
    },
    infoContainer: {
      paddingLeft: 10,
      width: "90%",
    },
    cityContainer: {
      flex: 1,
      flexDirection: "row",
      alignItems: "center",
      justifyContent: "flex-start",
    },
    naming: {
      color: "yellow",
      fontSize: 12,
      alignSelf: "flex-start",
    },
    devider: {
      borderBottomColor: "#999999",
      borderBottomWidth: 5,
      width: "85%",
    },
    date: {
      fontSize: 10,
      color: "white",
    },
  });
  