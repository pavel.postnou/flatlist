import { StyleSheet } from "react-native";

export const lightStyles = StyleSheet.create({
  title: {
    fontSize: 15,
    color: "black",
    marginRight: 20,
  },
  country: {
    fontSize: 15,
    color: "#2d56ad",
    marginRight: 20,
  },
  naming: {
    color: "#999999",
    fontSize: 12,
    alignSelf: "flex-start",
  },
  date: {
    fontSize: 10,
    color: "#e82323",
  },
  nameText: {
    fontSize: 30,
    color: "black",
  },
  describe: {
    color: "#999999",
    fontSize: 15,
  },
  description: {
    color: "black",
    fontSize: 17,
  },
  descriptionRed: {
    color: "#2d56ad",
    fontSize: 17,
  },
  error: {
    color:"red",
    fontSize: 25,
  }
});
