import { StyleSheet } from "react-native";

export const styles = StyleSheet.create({
    imgDescription: {
        flex: 1,
        resizeMode: "contain",
        marginVertical: 10,
        marginLeft:10,
        borderRadius: 10,
      },
      imgOne: {
        flex: 1,
        width: 80,
        resizeMode: "contain",
        marginVertical: 5,
        borderRadius: 10,
      },
      errorImg: {
        width:"60%",
        resizeMode: "contain",
      }
});
