import { Image} from "react-native";
import React from "react";
import { styles } from "./image.style";

export default function ImageComponent({ type, source,...rest }) {
  if (type == "imgDescription") return <Image style = {styles.imgDescription} source={source}/>
  else if (type == "imgOne") return <Image style = {styles.imgOne} source={source}/>
  else if (type == "errorImg") return <Image style = {styles.errorImg} source={source}/>
}