import { Text } from "react-native";
import React from "react";
import { darkStyles } from "./text.dark.style";
import { lightStyles } from "./text.light.style";

export default function TextComponent({ type, text, theme, ...rest }) {
    theme?styles=darkStyles:styles=lightStyles
  if (type == "title") return <Text style={styles.title}>{text}</Text>;
  else if (type == "naming") return <Text style={styles.naming}>{text}</Text>;
  else if (type == "country") return <Text style={styles.country}>{text}</Text>;
  else if (type == "date") return <Text style={styles.date}>{text}</Text>;
  else if (type == "nameText") return <Text style={styles.nameText}>{text}</Text>;
  else if (type == "description") return <Text style={styles.description}>{text}</Text>;
  else if (type == "describe") return <Text style={styles.describe}>{text}</Text>;
  else if (type == "descriptionRed") return <Text style={styles.descriptionRed}>{text}</Text>;
  else if (type == "error") return <Text style={styles.error}>{text}</Text>;
}