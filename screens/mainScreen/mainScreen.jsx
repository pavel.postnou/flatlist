import {
  FlatList,
  SafeAreaView,
  TextInput,
  ActivityIndicator,
  View,
  Button,
} from "react-native";
import * as RootNavigation from "../../navigation/RootNavigation";
import React, { useState, useEffect } from "react";
import Item from "../../components/Item";
import refreshFetch from "../../api/fetchRequest";
import { styles } from "./mainScreen.style";

export default function mainScreen() {
  let key = 0;
  let listItem = [];
  const [dark, setTheme] = useState(true);
  const [flatListItem, setFlatlistItem] = useState([]);
  const [search, setSearch] = useState("");
  const [users, setUsers] = useState(null);
  const [loading, setLoading] = useState(true);

  async function refresh() {
    setLoading(true);
    setUsers(null);
    const result = await refreshFetch();
    result
      ? (setUsers(result), setLoading(false))
      : (setLoading(false),
        RootNavigation.navigate("Error", { message: "Something went wrong" }));
  }

  useEffect(() => {
    refresh();
  }, []);

  function add() {
    for (let i = 0; i < users.results.length; i++) {
      listItem.push({
        id: key,
        name: users.results[i].name.first,
        image: users.results[i].picture.thumbnail,
        lastName: users.results[i].name.last,
        country: users.results[i].location.country,
        city: users.results[i].location.city,
        date: users.results[i].registered.date.substring(0, 10),
      });
      key++;
    }
    setFlatlistItem(listItem);
  }

  useEffect(() => {
    users ? add() : null;
  }, [users]);

  const renderItem = ({ item }) =>
    (item && item.name.toLowerCase().indexOf(search.toLowerCase()) !== -1) ||
    item.lastName.toLowerCase().indexOf(search.toLowerCase()) !== -1 ? (
      <Item data={item} users={users} theme={dark} />
    ) : null;

  function changeTheme() {
    setTheme(!dark);
  }

  return (
    <View style={styles.allContainer}>
      {!loading ? (
        <SafeAreaView style={styles.container}>
          <TextInput
            style={styles.input}
            onChangeText={(text) => setSearch(text)}
            value={search}
            placeholder="find"
          />
          <FlatList
            data={flatListItem}
            renderItem={renderItem}
            keyExtractor={(item) => item.id}
            refreshing={false}
            onRefresh={refresh}
          />
          <Button title="change theme" onPress={changeTheme} />
        </SafeAreaView>
      ) : (
        <ActivityIndicator size="large" color="blue" />
      )}
    </View>
  );
}
