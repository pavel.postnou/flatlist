import React from "react";
import { View, StyleSheet } from "react-native";
import TextComponent from "../../src/shared/components/Text";
import ImageComponent from "../../src/shared/components/Image";

export default function errorScreen({ route }) {
  return (
    <View style={styles.errorView}>
      <ImageComponent
        type="errorImg"
        source={require("../../assets/error.png")}
      />
      <TextComponent type="error" text={route.params.message} />
    </View>
  );
}

export const styles = StyleSheet.create({
errorView: {
    flex:1,
    alignItems:"center",
    justifyContent:"space-evenly"

}
})