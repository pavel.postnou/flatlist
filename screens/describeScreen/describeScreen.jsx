import { View } from "react-native";
import React from "react";
import { styles } from "./describeScreen.style";
import TextComponent from "../../src/shared/components/Text";
import ImageComponent from "../../src/shared/components/Image";

export default function describeScreen({route}) {
  let userInfo = route.params.send;
  return (
    <View style={route.params.theme?styles.user:styles.userLight}>
      <View style={styles.imgAndName}>
        <ImageComponent type = "imgDescription" source={{ uri: userInfo.picture.large }} />
        <View style={styles.name}>
          <TextComponent type={"describe"} theme={route.params.theme} text="Name: "/>
          <TextComponent type={"nameText"} theme={route.params.theme} text={userInfo.name.first} />
          <TextComponent type={"describe"} theme={route.params.theme} text="Last Name: "/>
          <TextComponent type={"nameText"} theme={route.params.theme} text={userInfo.name.last} />
          <TextComponent type={"describe"} theme={route.params.theme} text="Age: "/>
          <TextComponent type={"nameText"} theme={route.params.theme} text={userInfo.dob.age} />
        </View>
      </View>
      <View style={styles.allInfo}>
        <TextComponent
          type={"description"}
          theme={route.params.theme} 
          text={`Hello, i am ${userInfo.gender}, i live in ${
            userInfo.location.street.name
          } ${userInfo.location.street.number} city ${
            userInfo.location.city
          }, state ${userInfo.location.state}, in ${
            userInfo.location.country
          }. I am ${
            userInfo.dob.age
          } age old. My date of birth is ${userInfo.dob.date.slice(
            0,
            10
          )}. I will be glad to talk.`}
        />
        <TextComponent type={"descriptionRed"} theme={route.params.theme} text="email: " />
        <TextComponent type={"description"} theme={route.params.theme} text={userInfo.email} />
        <TextComponent type={"descriptionRed"} theme={route.params.theme} text="phone: " />
        <TextComponent type={"description"} theme={route.params.theme} text={userInfo.phone} />
      </View>
    </View>
  );
}
